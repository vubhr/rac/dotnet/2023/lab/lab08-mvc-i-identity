namespace TodoAppMT.Models;

public class Todo {
    public int Id { get; set; }
    public string Title { get; set; }
    public bool IsDone { get; set; }

    public string? IdentityUserId { get; set; }
    public IdentityUser? IdentityUser { get; set; }
}